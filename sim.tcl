set sim sim_cpu
update_compile_order -fileset sources_1
update_compile_order -fileset $sim
launch_simulation -simset $sim
# make simulation complete
run 100 us
exit
