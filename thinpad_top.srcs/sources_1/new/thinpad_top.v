`default_nettype none

module thinpad_top(
    input wire clk_50M,           //50MHz 时钟输入
    input wire clk_11M0592,       //11.0592MHz 时钟输入（备用，可不用）

    input wire clock_btn,         //BTN5手动时钟按钮开关，带消抖电路，按下时为1
    input wire reset_btn,         //BTN6手动复位按钮开关，带消抖电路，按下时为1

    input  wire[3:0]  touch_btn,  //BTN1~BTN4，按钮开关，按下时为1
    input  wire[31:0] dip_sw,     //32位拨码开关，拨到“ON”时为1
    output reg [15:0] leds,       //16位LED，输出时1点亮
    output wire[7:0]  dpy0,       //数码管低位信号，包括小数点，输出1点亮
    output wire[7:0]  dpy1,       //数码管高位信号，包括小数点，输出1点亮

    //CPLD串口控制器信号
    output reg uart_rdn,         //读串口信号，低有效
    output reg uart_wrn,         //写串口信号，低有效
    input wire uart_dataready,    //串口数据准备好
    input wire uart_tbre,         //发送数据标志
    input wire uart_tsre,         //数据发送完毕标志

    //BaseRAM信号
    inout wire[31:0] base_ram_data,  //BaseRAM数据，低8位与CPLD串口控制器共享
    output wire[19:0] base_ram_addr, //BaseRAM地址
    output wire[3:0] base_ram_be_n,  //BaseRAM字节使能，低有效。如果不使用字节使能，请保持为0
    output reg base_ram_ce_n,       //BaseRAM片选，低有效
    output reg base_ram_oe_n,       //BaseRAM读使能，低有效
    output reg base_ram_we_n,       //BaseRAM写使能，低有效

    //ExtRAM信号
    inout wire[31:0] ext_ram_data,  //ExtRAM数据
    output wire[19:0] ext_ram_addr, //ExtRAM地址
    output wire[3:0] ext_ram_be_n,  //ExtRAM字节使能，低有效。如果不使用字节使能，请保持为0
    output wire ext_ram_ce_n,       //ExtRAM片选，低有效
    output wire ext_ram_oe_n,       //ExtRAM读使能，低有效
    output wire ext_ram_we_n,       //ExtRAM写使能，低有效

    //直连串口信号
    output wire txd,  //直连串口发送端
    input  wire rxd,  //直连串口接收端

    //Flash存储器信号，参考 JS28F640 芯片手册
    output wire [22:0]flash_a,      //Flash地址，a0仅在8bit模式有效，16bit模式无意义
    inout  wire [15:0]flash_d,      //Flash数据
    output wire flash_rp_n,         //Flash复位信号，低有效
    output wire flash_vpen,         //Flash写保护信号，低电平时不能擦除、烧写
    output wire flash_ce_n,         //Flash片选信号，低有效
    output wire flash_oe_n,         //Flash读使能信号，低有效
    output wire flash_we_n,         //Flash写使能信号，低有效
    output wire flash_byte_n,       //Flash 8bit模式选择，低有效。在使用flash的16位模式时请设为1

    //USB 控制器信号，参考 SL811 芯片手册
    output wire sl811_a0,
    //inout  wire[7:0] sl811_d,     //USB数据线与网络控制器的dm9k_sd[7:0]共享
    output wire sl811_wr_n,
    output wire sl811_rd_n,
    output wire sl811_cs_n,
    output wire sl811_rst_n,
    output wire sl811_dack_n,
    input  wire sl811_intrq,
    input  wire sl811_drq_n,

    //网络控制器信号，参考 DM9000A 芯片手册
    output wire dm9k_cmd,
    inout  wire[15:0] dm9k_sd,
    output wire dm9k_iow_n,
    output wire dm9k_ior_n,
    output wire dm9k_cs_n,
    output wire dm9k_pwrst_n,
    input  wire dm9k_int,

    //图像输出信号
    output wire[2:0] video_red,    //红色像素，3位
    output wire[2:0] video_green,  //绿色像素，3位
    output wire[1:0] video_blue,   //蓝色像素，2位
    output wire video_hsync,       //行同步（水平同步）信号
    output wire video_vsync,       //场同步（垂直同步）信号
    output wire video_clk,         //像素时钟输出
    output wire video_de           //行数据有效信号，用于区分消隐区
);

/* =========== Demo code begin =========== */

// PLL分频示例
wire locked, clk_10M, clk_20M;
pll_example clock_gen 
 (
  // Clock out ports
  .clk_out1(clk_10M), // 时钟输出1，频率在IP配置界面中设置
  .clk_out2(clk_20M), // 时钟输出2，频率在IP配置界面中设置
  // Status and control signals
  .reset(reset_btn), // PLL复位输入
  .locked(locked), // 锁定输出，"1"表示时钟稳定，可作为后级电路复位
 // Clock in ports
  .clk_in1(clk_50M) // 外部时钟输入
 );

// 7段数码管译码器演示，将number用16进制显示在数码管上面
reg[7:0] number;
SEG7_LUT segL(.oSEG1(dpy0), .iDIG(number[3:0])); //dpy0是低位数码管
SEG7_LUT segH(.oSEG1(dpy1), .iDIG(number[7:4])); //dpy1是高位数码管

reg reset_of_clk10M;
// 异步复位，同步释放
always@(posedge clk_10M or negedge locked) begin
    if(~locked) reset_of_clk10M <= 1'b1;
    else        reset_of_clk10M <= 1'b0;
end

reg [3:0] state, cnt;
reg [7:0] address, data;
reg data_bus_t;

always@(posedge clk_10M or posedge reset_of_clk10M) begin
    if(reset_of_clk10M)begin
        state <= 0;
        uart_rdn <= 1;
        uart_wrn <= 1;
        base_ram_ce_n <= 0;
        base_ram_oe_n <= 1;
        base_ram_we_n <= 1;
        data_bus_t <= 1;
    end
    else begin
        number <= {cnt, state};
        case(state)
        4'h0: begin
            address <= dip_sw[7:0];
            cnt <= 0;
            state <= 4'h1;
        end
        4'h1: begin
            if(uart_dataready) begin
                uart_rdn <= 1'b0;
                state <= 4'h3;
            end
        end
        4'h3: begin
            data <= base_ram_data[7:0];
            uart_rdn <= 1;
            state <= 4'h4;
        end
        4'h4: begin
            base_ram_we_n <= 0;
            data_bus_t <= 0;
            state <= 4'h5;
        end
        4'h5: begin
            base_ram_we_n <= 1;
            data_bus_t <= 1;
            state <= 4'h6;
        end
        4'h6: begin
            if(cnt == 9)
                state <= 4'h7;
            else
                state <= 4'h1;
            cnt <= cnt+1;
            address <= address+1;
        end
        4'h7: begin
            address <= dip_sw[7:0];
            cnt <= 0;
            state <= 4'h8;
        end
        4'h8: begin
            base_ram_oe_n <= 0;
            state <= 4'h9;
        end
        4'h9: begin
            data <= base_ram_data[7:0];
            base_ram_oe_n <= 1;
            state <= 4'ha;
        end
        4'ha: begin
            uart_wrn <= 0;
            data_bus_t <= 0;
            state <= 4'hb;
        end
        4'hb: begin
            uart_wrn <= 1;
            data_bus_t <= 1;
            state <= 4'hc;
        end
        4'hc: begin
            if(!uart_tsre||!uart_tbre);
            else begin
                if(cnt == 9)
                    state <= 4'hd;
                else
                    state <= 4'h8;
                cnt <= cnt+1;
                address <= address+1;
            end
        end
        4'hd: begin

        end
        endcase
    end
end

assign base_ram_data = data_bus_t ? 32'bz : data;
assign base_ram_addr = address;
assign base_ram_be_n = 4'h0;

assign ext_ram_ce_n = 1'b1;
assign ext_ram_oe_n = 1'b1;
assign ext_ram_we_n = 1'b1;

// led tests

reg[3:0] bd_cnt;
reg sw31_last;
always@(posedge clk_10M or posedge reset_of_clk10M) begin
    if(reset_of_clk10M)begin
        bd_cnt <= 0;
    end else begin
        if(dip_sw[31] & ~sw31_last)
            bd_cnt <= bd_cnt+1;
        if(bd_cnt >= 4) begin
            leds <= dip_sw;
        end
        sw31_last <= dip_sw[31];
    end
end

endmodule
