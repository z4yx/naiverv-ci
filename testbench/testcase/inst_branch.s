   .org 0x0
   .global _start
_start:
   ori  x3,x0,0x1
   slli x3,x3,31            # x3 = 0x80000000
   ori  x1,x0,0x0001        # x1 = 0x1                
   ori  x1,x0,0x0002        # x1 = 0x2
   j    s1
1:
   ori  x1,x0,0x111
   ori  x1,x0,0x110

   .org 0x20
s1:
   ori  x1,x0,0x0003        # x1 = 0x3   
   nop       
   jal  x31,s2
   ori  x1,x0,0x110
   ori  x1,x0,0x111
   bne  x1,x0,s3
   nop
   ori  x1,x0,0x110
   ori  x1,x0,0x111

   .org 0x50   
s2:
   ori  x1,x0,0x0004      # x1 = 0x4
   or   x1,x31,x0         # x1 = 0x8000002c
   beq  x1,x1,s3           

   ori  x1,x0,0x111
   ori  x1,x0,0x110
2:
   ori  x1,x0,0x0007      # x1 = 0x7
   ori  x1,x0,0x0008      # x1 = 0x8
   ori  x1,x0,0x0009      # x1 = 0x9
   or   x2,x1,x0          # x2 = 0x9
   beq  x1,x2,s4
   ori  x1,x0,0x111
   ori  x1,x0,0x110

   .org 0x80
s3:
   ori  x1,x0,0x0005      # x1 = 0x5            
   ori  x1,x0,0x0006      # x1 = 0x6
   bne  x1,x31,2b
   ori  x1,x0,0x111
   ori  x1,x0,0x110

   .org 0x130
s5:
   auipc x2,5             # x2 = 0x80005130
   ori  x2,x0,0x10
   ori  x1,x0,0x000d      # x1 = 0xd
   ori  x1,x0,0x000e      # x1 = 0xe
   ori  x1,x0,0x000f      # x1 = 0xf
   ori  x1,x0,0x0010      # x1 = 0x10            
   bne  x1,x2,s4         
   ori  x1,x0,0x0011      # x1 = 0x11
   bne  x1,x2,s6       
   ori  x1,x0,0x110

   .org 0x160
s4:
   ori  x1,x0,0x000a      # x1 = 0xa              
   beq  x1,x2,s3
   ori  x1,x0,0x000b      # x1 = 0xb
   ori  x1,x0,0x000c      # x1 = 0xc
   beq  x0,x0,s5
   ori  x1,x0,0x110

   .org 0x190
s6:
   ori  x1,x0,0x0012      # x1 = 0x12
   ori  x1,x0,0x0013      # x1 = 0x13
   ori x1,x0,0x0014       # x1 = 0x14
   nop
   
_loop:
   j _loop
   nop
