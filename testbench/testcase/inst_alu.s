   .org 0x0
   .global _start
_start:

   ######### add\sub\and\or\xor\sll\srl ##########

   ori  x1,x0,0x80             # x1 = 0x80
   slli x1,x1,24               # x1 = 0x80000000
   ori  x1,x1,0x0010           # x1 = 0x80000010

   ori  x2,x0,-0x800           # x2 = 0xfffff800
   slli x2,x2,20               # x2 = 0x80000000
   ori  x2,x2,0x0001           # x2 = 0x80000001

   ori  x3,x0,0x0000           # x3 = 0x00000000
   add  x3,x2,x1               # x3 = 0x00000011
   ori  x3,x0,0x0000           # x3 = 0x00000000

   ori  x3,x0,0x0000           # x3 = 0x00000000
   addi x3,x3,-0x800           # x3 = 0xfffff800

   or   x4,x3,x2               # x4 = 0xfffff801
   xor  x5,x3,x2               # x5 = 0x7ffff801
   xori x5,x5,0x11             # x5 = 0x7ffff810
   
   and  x6,x4,x5               # x6 = 0x7ffff800
   andi x7,x5,-0x800           # x7 = 0x7ffff800
   xor  x6,x6,x7               # x6 = 0x00000000
   srli x6,x4,4                # x6 = 0x0fffff80

   lui  x6,0x12345             # x6 = 0x12345000
   addi x7,x6,-1               # x7 = 0x12344fff
   sub  x6,x6,x7               # x6 = 0x1

   nop
   nop

