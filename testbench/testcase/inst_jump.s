   .org 0x0
   .global _start
_start:
   ori  x1,x0,0x0001   # x1 = 0x1                
   ori  x1,x0,0x0002   # x1 = 0x2
   j    b20
   j    b40
   ori  x1,x0,0x111
   ori  x1,x0,0x110

   .org 0x20
b20:
   ori  x1,x0,0x0003   # x1 = 0x3               
   nop
   jal  x31,b40
   ori  x1,x0,0x0005   # x1 = 0x5
   ori  x1,x0,0x0006   # x1 = 0x6
   j    b60

   .org 0x40
b40:   
   ori  x1,x0,4         # x1 = 0x4
   jalr x0,0(x31)          
next:
   ori  x1,x0,0x0009    # x1 = 0x9
   ori  x1,x0,0x000a    # x1 = 0xa
   lui  x10,0x80000     # x10 = 0x80000000
   ori  x10,x10,0x90    # x10 = 0x80000090
   jalr x0,-8(x10)      # goto b88
   jal  x10,b98

   .org 0x60
b60:
   ori  x1,x0,0x0007    # x1 = 0x7                
   ori  x1,x0,0x0008    # x1 = 0x8
   j    next           
   ori  x1,x0,0x111
   ori  x1,x0,0x110

   .org 0x80
   ori  x1,x0,0x110
   .org 0x88
b88:
   jalr x30,0x8(x10)    # goto b98
   ori  x1,x0,0x000b    # x1 = 0xb
   j _loop

   .org 0x98
b98:
   jalr x30,0(x30)       # return
   ori  x1,x0,0x110

_loop:
   j _loop
   nop
