   .org 0x0
   .global _start
_start:
   lui  s0,0x80001       # $8 = 0x80001000
   ori  t3,x0,-0x10f     # $28= 0xfffffef1

   sb   t3,0x3(s0)       # [0x3] = 0xf1
   srli t3,t3,8
   sb   t3,0x0(s0)       # [0x0] = 0xfe
   ori  t3,x0,0x1cd
   sb   t3,0x1(s0)       # [0x1] = 0xcd
   slli t3,t3,16
   sb   t3,0x2(s0)       # [0x2] = 0x00

   lb   t6,0x3(s0)       # t6 = 0xfffffff1
   # ALU execution after load
   xori t6,t6,1          # t6 = 0xfffffff0

   ori  t3,x0,-0x555
   sh   t3,0x4(s0)
   lh   t6,0x4(s0)       # t6 = 0xfffffaab

   ori  t3,x0,-0x767
   sh   t3,0x6(s0)
   lh   t6,0x6(s0)       # t6 = 0xfffff899

   ori  t3,x0,0x455
   slli t3,t3,0x10
   ori  t3,t3,0x667
   sw   t3,0x8(s0)
   lw   t6,0x8(s0)       # t6 = 0x04550667

   lw   t6,0x4(s0)       # t6 = 0xf899faab
   lw   t6,0x0(s0)       # t6 = 0xf100cdfe

   lw  t5,0x0(s0)        # t5 = 0xf100cdfe
   beq t6,t5,Label       # branch after load
   ori t6,x0,0x456
   j _loop

Label:
   ori t5,x0,0         # t5 = 0x00000000
   lw  t6,0x0(s0)      # t6 = 0xf100cdfe
   lw  t5,0x0(s0)      # t5 = 0xf100cdfe
   addi t5,t5,1        # ALU execution after load
   beq t6,t5,Label2
   ori t6,x0,0x555
   j _loop

Label2:
   ori t6,x0,0x666

_loop:
   j _loop
   nop
