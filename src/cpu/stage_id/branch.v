module branch(/*autoport*/
//output
          is_branch,
          branch_taken,
          branch_address,
          return_address,
//input
          inst,
          pc_value,
          reg_s_value,
          reg_t_value);

input wire[31:0] inst;
input wire[31:0] pc_value;
input wire[31:0] reg_s_value;
input wire[31:0] reg_t_value;
output reg is_branch;
output reg branch_taken;
output reg[31:0] branch_address;
output reg[31:0] return_address;

wire[31:0] temp_j_ext, temp_i_ext, temp_b_ext;
wire sign;
assign sign = inst[31];
assign temp_j_ext = {
    sign,sign,sign,sign,sign,sign,
    sign,sign,sign,sign,sign,sign,
    inst[19:12],inst[20],inst[30:21],1'b0
    };
assign temp_i_ext = {
    sign,sign,sign,sign,sign,sign,sign,sign,sign,sign,
    sign,sign,sign,sign,sign,sign,sign,sign,sign,sign,
    inst[31:20]
    };
assign temp_b_ext = {
    sign,sign,sign,sign,sign,sign,sign,sign,sign,sign,
    sign,sign,sign,sign,sign,sign,sign,sign,sign,sign,
    inst[7],inst[30:25],inst[11:8],1'b0
    };

always @(*) begin
    branch_address <= 32'b0;
    is_branch <= 1'b0;
    branch_taken <= 1'b0;
    return_address <= pc_value + 32'd4;
    casez({inst[14:12],inst[6:0]})
    10'b000_1100111: begin //JR, JALR
        is_branch <= 1'b1;
        branch_address <= reg_s_value + temp_i_ext;
        branch_taken <= 1'b1;
    end
    10'b???1101111: begin //J, JAL
        is_branch <= 1'b1;
        branch_address <= pc_value + temp_j_ext;
        branch_taken <= 1'b1;
    end
    10'b000_1100011: begin //BEQ
        is_branch <= 1'b1;
        branch_address <= pc_value + temp_b_ext;
        if(reg_s_value == reg_t_value) begin
            branch_taken <= 1'b1;
        end
    end
    10'b001_1100011: begin //BNE
        is_branch <= 1'b1;
        branch_address <= pc_value + temp_b_ext;
        if(reg_s_value != reg_t_value) begin
            branch_taken <= 1'b1;
        end
    end
    endcase
end

endmodule