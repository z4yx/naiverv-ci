`include "../defs.v"
module id_r(/*autoport*/
//output
            op,
//input
            inst);

input wire[31:0] inst;
output reg[7:0] op;

always @(*) begin
    case(inst[6:0])
    7'b0110011: begin
        case({inst[31:25],inst[14:12]})
        10'b0000000_000: op <= `OP_ADD;
        10'b0100000_000: op <= `OP_SUB;
        10'b0000000_001: op <= `OP_SLL;
        10'b0000000_100: op <= `OP_XOR;
        10'b0000000_101: op <= `OP_SRL;
        10'b0000000_110: op <= `OP_OR;
        10'b0000000_111: op <= `OP_AND;
        default: op <= `OP_INVAILD;
        endcase
    end
    default: op <= `OP_INVAILD;
    endcase
end


endmodule