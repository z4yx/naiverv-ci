`include "../defs.v"
module id_i(/*autoport*/
//output
            op,
            immediate,
//input
            inst);


input wire[31:0] inst;
output reg[7:0] op;
output reg[19:0] immediate;

always @(*) begin
    case(inst[6:0])
    7'b0000011: begin
        immediate <= {8'b0, inst[31:20]};
        case(inst[14:12])
        3'b000: op <= `OP_LB;
        3'b001: op <= `OP_LH;
        3'b010: op <= `OP_LW;
        default: op <= `OP_INVAILD;
        endcase
    end
    7'b0100011: begin
        immediate <= {8'b0, inst[31:25], inst[11:7]};
        case(inst[14:12])
        3'b000: op <= `OP_SB;
        3'b001: op <= `OP_SH;
        3'b010: op <= `OP_SW;
        default: op <= `OP_INVAILD;
        endcase
    end
    7'b1100011: begin
        immediate <= 20'b0;
        case(inst[14:12])
        3'b000: op <= `OP_BEQ;
        3'b001: op <= `OP_BNE;
        default: op <= `OP_INVAILD;
        endcase
    end
    7'b0010011: begin
        immediate <= {8'b0, inst[31:20]};
        case(inst[14:12])
        3'b000: op <= `OP_ADD;
        3'b001: op <= `OP_SLL;
        3'b100: op <= `OP_XOR;
        3'b101: op <= `OP_SRL;
        3'b110: op <= `OP_OR;
        3'b111: op <= `OP_AND;
        default: op <= `OP_INVAILD;
        endcase
    end
    7'b0010111: begin
        immediate <= inst[31:12];
        op <= `OP_AUIPC;
    end
    7'b0110111: begin
        immediate <= inst[31:12];
        op <= `OP_LU;
    end
    7'b1101111: begin
        immediate <= 20'b0;
        op <= `OP_JAL;
    end
    7'b1100111: begin
        immediate <= 20'b0;
        op <= `OP_JALR;
    end
    default: op <= `OP_INVAILD;
    endcase
end


endmodule