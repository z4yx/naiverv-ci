`include "../defs.v"
`default_nettype none
module id(/*autoport*/
//output
          op,
          op_type,
          reg_s,
          reg_t,
          reg_d,
          immediate,
//input
          inst,
          pc_value);

input wire[31:0] inst;
input wire[31:0] pc_value;

output reg[7:0] op;
output reg[1:0] op_type;
output wire[4:0] reg_s;
output wire[4:0] reg_t;
output wire[4:0] reg_d;
output wire[19:0] immediate;

wire [7:0] id_i_op;
id_i id_i_inst(
    .op(id_i_op),
    .immediate(immediate),
    .inst(inst)
);

wire [7:0] id_r_op;
id_r id_r_inst(
    .op(id_r_op),
    .inst(inst)
);

always @(*) begin
    if(id_i_op != `OP_INVAILD)
        op_type <= `OPTYPE_I;
    else if(id_r_op != `OP_INVAILD)
        op_type <= `OPTYPE_R;
    else
        op_type <= `OPTYPE_INVALID;
end

always @(*) begin
    case(op_type)
    `OPTYPE_I: begin
        op <= id_i_op;
    end
    `OPTYPE_R: begin
        op <= id_r_op;
    end
    default: begin
        op <= `OP_INVAILD;
    end
    endcase
end

assign reg_d = inst[11:7];
assign reg_s = inst[19:15];
assign reg_t = inst[24:20];

endmodule