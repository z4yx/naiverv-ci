`include "../defs.v"
module wb(/*autoport*/
//output
          reg_we,
          data_o,
//input
          mem_access_op,
          mem_access_sz,
          mem_data_i,
          addr_i,
          data_i,
          reg_addr_i,
          flag_unsigned);

input wire[1:0] mem_access_op;
input wire[2:0] mem_access_sz;
input wire[31:0] mem_data_i;
input wire[31:0] addr_i;
input wire[31:0] data_i;
input wire[4:0] reg_addr_i;
input wire flag_unsigned;

output wire reg_we;
output reg[31:0] data_o;

reg[7:0] data_i_byte;
reg[15:0] data_i_half;
wire[7:0] sign_byte,sign_half;

assign sign_half = {data_i_half[15],data_i_half[15],data_i_half[15],data_i_half[15],
    data_i_half[15],data_i_half[15],data_i_half[15],data_i_half[15]};
assign sign_byte = {data_i_byte[7],data_i_byte[7],data_i_byte[7],data_i_byte[7],
    data_i_byte[7],data_i_byte[7],data_i_byte[7],data_i_byte[7]};

assign reg_we = ((mem_access_op==`ACCESS_OP_M2R)||(mem_access_op==`ACCESS_OP_D2R));

always @(*) begin
    data_i_half = addr_i[1] ? mem_data_i[31:16] : mem_data_i[15:0];
    case(addr_i[1:0])
    2'b11: begin data_i_byte = mem_data_i[31:24]; end
    2'b10: begin data_i_byte = mem_data_i[23:16]; end
    2'b01: begin data_i_byte = mem_data_i[15:8]; end
    2'b00: begin data_i_byte = mem_data_i[7:0]; end
    endcase
end

always @(*) begin
    case(mem_access_op)
    `ACCESS_OP_M2R: begin
        if(mem_access_sz==`ACCESS_SZ_WORD)
            data_o = mem_data_i;
        else if(mem_access_sz==`ACCESS_SZ_HALF)
            data_o = flag_unsigned ? {16'b0,data_i_half} : {sign_half,sign_half,data_i_half};
        else if(mem_access_sz==`ACCESS_SZ_BYTE)
            data_o = flag_unsigned ? {24'b0,data_i_byte} : {sign_byte,sign_byte,sign_byte,data_i_byte};
        else
            data_o = 32'b0;
    end
    default: begin
        data_o = data_i;
    end
    endcase
end

endmodule
