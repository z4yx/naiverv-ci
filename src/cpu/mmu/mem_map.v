
module mem_map(/*autoport*/
//output
         addr_o,
//input
         addr_i,
         en);

input wire[31:0] addr_i;
output reg[31:0] addr_o;
input wire en;

always @(*) begin
    addr_o <= {1'b0, addr_i[30:0]};
end

endmodule
