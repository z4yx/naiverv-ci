`include "../defs.v"
`default_nettype none
module ex(/*autoport*/
//output
          mem_access_op,
          mem_access_sz,
          data_o,
          mem_addr,
          reg_addr,
          stall,
//input
          clk,
          rst_n,
          op,
          op_type,
          address,
          pc_value,
          reg_s,
          reg_t,
          reg_d,
          reg_s_value,
          reg_t_value,
          immediate);

input wire clk;
input wire rst_n;

input wire[7:0] op;
input wire[1:0] op_type;
input wire[31:0] address;
input wire[4:0] reg_s;
input wire[4:0] reg_t;
input wire[4:0] reg_d;
input wire[31:0] reg_s_value;
input wire[31:0] pc_value;
input wire[31:0] reg_t_value;
input wire[19:0] immediate;

output reg[1:0] mem_access_op;
output reg[2:0] mem_access_sz;
output reg[31:0] data_o;
output reg[31:0] mem_addr;
output reg[4:0] reg_addr;
output wire stall;

wire [31:0] tmp_sign_operand, tmp_add, tmp_sub;
wire [31:0] signExtImm;

wire imm_s;

assign stall = 1'b0;

assign imm_s = immediate[11];
assign signExtImm = { imm_s,imm_s,imm_s,imm_s,imm_s,imm_s,imm_s,imm_s,imm_s,imm_s,imm_s,imm_s,imm_s,imm_s,imm_s,imm_s, 
                      imm_s,imm_s,imm_s,imm_s, immediate[11:0]};

assign tmp_sign_operand = (op_type==`OPTYPE_R ? reg_t_value : signExtImm);
assign tmp_add = reg_s_value + tmp_sign_operand;
assign tmp_sub = reg_s_value - tmp_sign_operand; //used by SLT/SLTI and SUB

always @(*) begin
    reg_addr <= reg_d;
    case (op)
    `OP_ADD: begin
        data_o <= tmp_add;
    end
    `OP_AND: begin
        data_o <= (op_type==`OPTYPE_R) ? reg_s_value&reg_t_value : reg_s_value&signExtImm;
    end
    `OP_JAL,`OP_JALR: begin
        data_o <= address;
    end
    `OP_LB,`OP_LH,`OP_LW: begin
        data_o <= reg_t_value;
    end
    `OP_LU: begin
        data_o <= {immediate, 12'b0};
    end
    `OP_OR: begin
        data_o <= (op_type==`OPTYPE_R) ? reg_s_value|reg_t_value : reg_s_value|signExtImm;
    end
    `OP_XOR: begin
        data_o <= (op_type==`OPTYPE_R) ? reg_s_value^reg_t_value : reg_s_value^signExtImm;
    end
    `OP_SLL: begin
        data_o <= reg_s_value<<immediate[4:0];
    end
    `OP_SRL: begin
        data_o <= reg_s_value>>immediate[4:0];
    end
    `OP_SB,`OP_SH,`OP_SW: begin
        data_o <= reg_t_value;
    end
    `OP_SUB: begin
        data_o <= tmp_sub;
    end
    `OP_AUIPC: begin
        data_o <= pc_value+{immediate, 12'b0};
    end
    default: begin
        data_o <= 32'h0;
        reg_addr <= 5'h0;
    end
    endcase
end

always @(*) begin
    case (op)
    `OP_LB,
    `OP_LH,
    `OP_LW: begin
        mem_addr <= reg_s_value+signExtImm;
        mem_access_op <= `ACCESS_OP_M2R;
     end
    `OP_SB,
    `OP_SH,
    `OP_SW: begin
        mem_addr <= reg_s_value+signExtImm;
        mem_access_op <= `ACCESS_OP_R2M;
    end
    default: begin
        mem_addr <= 32'b0;
        mem_access_op <= `ACCESS_OP_D2R;
    end
    endcase
end

always @(*) begin
    case (op)
    `OP_LB,
    `OP_SB:
        mem_access_sz <= `ACCESS_SZ_BYTE;
    `OP_LH,
    `OP_SH:
        mem_access_sz <= `ACCESS_SZ_HALF;
    default:
        mem_access_sz <= `ACCESS_SZ_WORD;
    endcase
end
endmodule