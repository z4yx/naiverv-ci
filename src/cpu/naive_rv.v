`include "defs.v"
`default_nettype none

module naive_rv(/*autoport*/
//output
      ibus_address,
      ibus_byteenable,
      ibus_read,
      dbus_address,
      dbus_byteenable,
      dbus_read,
      dbus_write,
      dbus_wrdata,
//input
      rst_n,
      clk,
      ibus_rddata,
      ibus_stall,
      dbus_rddata,
      dbus_stall,
      hardware_int_in);

parameter BUS_READ_1CYCLE = 0;

input wire rst_n;
input wire clk;

output wire[31:0] ibus_address;
output wire[3:0] ibus_byteenable;
output wire ibus_read;
input wire[31:0] ibus_rddata;
input wire ibus_stall;

output wire[31:0] dbus_address;
output wire[3:0] dbus_byteenable;
output wire dbus_read;
output wire dbus_write;
output wire[31:0] dbus_wrdata;
input wire[31:0] dbus_rddata;
input wire dbus_stall;

input wire[4:0] hardware_int_in;

reg flush;
reg en_pc,en_ifid,en_idex,en_exmm,en_mmwb;

wire [31:0]if_pc;
wire [31:0]if_iaddr_phy;
wire if_valid_iaddr;
reg if_read_hold;
reg [31:0]if_iaddr_phy_hold;

wire [19:0]id_immediate;
wire [1:0]id_op_type;
wire [7:0]id_op;
reg [31:0]id_inst, id_ibus_rddata;
reg [31:0]id_inst_keep;
wire [4:0]id_reg_s;
wire [4:0]id_reg_d;
wire [31:0]id_address;
wire [4:0]id_reg_t;
wire [31:0]id_branch_address;
wire id_is_branch;
wire id_branch_taken;
reg id_cancel_instr;
reg [31:0]id_pc_value;
reg id_real_inst;
reg id_valid_iaddr;
reg id_update_flag,id_update_flag_last;

wire [31:0] id_reg_s_value_from_regs, id_reg_t_value_from_regs;
wire [31:0] id_reg_s_value, id_reg_t_value;

reg [19:0]ex_immediate;
reg [1:0]ex_op_type;
reg [7:0]ex_op;
reg [4:0]ex_reg_s;
reg [4:0]ex_reg_d;
reg [31:0]ex_address;
reg [4:0]ex_reg_t;
reg [31:0]ex_reg_s_value;
reg [31:0]ex_reg_t_value;
wire [1:0]ex_mem_access_op;
wire [2:0]ex_mem_access_sz;
wire [31:0]ex_data_o;
wire [31:0]ex_mem_addr;
wire [4:0]ex_reg_addr;
wire ex_stall;
reg [31:0]ex_pc_value;
reg ex_real_inst;

wire mm_mem_wr;
reg mm_invalid_inst;
wire [31:0]mm_mem_data_o;
reg [2:0]mm_mem_access_sz;
reg [4:0]mm_reg_addr_i;
wire [31:0]mm_mem_address;
wire [3:0]mm_mem_byte_en;
wire mm_mem_rd;
reg [1:0]mm_mem_access_op;
reg [31:0]mm_data_i;
wire [31:0]mm_data_o;
reg [31:0]mm_addr_i;
reg [31:0]mm_pc_value;
reg mm_real_inst;
wire mm_alignment_err;
wire mm_stall;


wire wb_reg_we;
reg [31:0]wb_addr_i;
reg [31:0]wb_data_i, wb_mem_data_i;
reg [31:0]wb_dbus_rddata;
wire[31:0]wb_data_o;
reg [2:0]wb_mem_access_sz;
reg [1:0]wb_mem_access_op;
reg [4:0]wb_reg_addr_i;

regs main_regs(/*autoinst*/
         .rdata1(id_reg_s_value_from_regs),
         .rdata2(id_reg_t_value_from_regs),
         .clk(clk),
         .rst_n(rst_n),
         .we(wb_reg_we),
         .waddr(wb_reg_addr_i),
         .wdata(wb_data_o),
         .raddr1(id_reg_s),
         .raddr2(id_reg_t));

mmu_top mmu(/*autoinst*/
      .data_address_o(dbus_address),
      .inst_address_o(if_iaddr_phy),
      .rst_n(rst_n),
      .clk(clk),
      .data_address_i(mm_mem_address),
      .inst_address_i(if_pc),
      .data_en(mm_mem_rd | mm_mem_wr),
      .inst_en(1'b1));

assign ibus_address = if_read_hold ? if_iaddr_phy_hold : if_iaddr_phy;
assign ibus_byteenable = 4'b1111;
assign ibus_read = if_valid_iaddr|if_read_hold; //keep ibus read signal asserted while ibus stall
assign if_valid_iaddr = 1'b1;

assign dbus_byteenable = mm_mem_byte_en;
assign dbus_read = mm_mem_rd;
assign dbus_write = mm_mem_wr;
assign dbus_wrdata= mm_mem_data_o;
assign mm_stall = dbus_stall;

always @(posedge clk) begin
    if (!rst_n) begin
        flush <= 1'b0;
    end
end

always @(*) begin
    if(mm_stall) begin
        {en_pc,en_ifid,en_idex,en_exmm,en_mmwb} <= 5'b00000;
    end else if(ex_mem_access_op == `ACCESS_OP_M2R &&
      (ex_reg_addr == id_reg_s || ex_reg_addr == id_reg_t)) begin
        {en_pc,en_ifid,en_idex,en_exmm,en_mmwb} <= 5'b00011;
    end else if(mm_mem_access_op == `ACCESS_OP_M2R &&
      (mm_reg_addr_i == id_reg_s || mm_reg_addr_i == id_reg_t)) begin
        {en_pc,en_ifid,en_idex,en_exmm,en_mmwb} <= 5'b00011;
    end else if(ibus_stall) begin
        {en_pc,en_ifid,en_idex,en_exmm,en_mmwb} <= 5'b00011;
    end else begin
        {en_pc,en_ifid,en_idex,en_exmm,en_mmwb} <= 5'b11111;
    end
end

pc pc_instance(/*autoinst*/
         .pc_reg(if_pc),
         .rst_n(rst_n),
         .clk(clk),
         .enable(en_pc),
         .is_exception(flush),
         .exception_new_pc(32'h0),
         .is_branch(id_branch_taken),
         .branch_address(id_branch_address));

always @(posedge clk) begin : proc_if_read_hold
    if(~rst_n) begin
        if_read_hold <= 1'b0;
    end else begin
        if_read_hold <= ibus_read & ibus_stall;
        if(!if_read_hold && ibus_read) //first cycle of ibus read transaction
            if_iaddr_phy_hold <= if_iaddr_phy;
    end
end

always @(posedge clk) begin
    if (!rst_n || (!en_ifid && en_idex) || flush) begin
        id_pc_value <= 32'b0;
        id_ibus_rddata <= 32'h0;
        id_real_inst <= 1'b0;
        id_valid_iaddr <= 1'b0;
        id_cancel_instr <= 1'b0;
        id_update_flag <= 1'b0;
    end
    else if(en_ifid) begin
        id_pc_value <= if_pc;
        id_ibus_rddata <= ibus_rddata;
        id_real_inst <= 1'b1;
        id_valid_iaddr <= if_valid_iaddr;
        id_cancel_instr <= id_branch_taken;
        id_update_flag <= ~id_update_flag;
    end
end

always @(posedge clk) begin
    id_update_flag_last <= id_update_flag;
    if(id_update_flag != id_update_flag_last)
      id_inst_keep <= id_inst;
end


always @(*) begin
    if(BUS_READ_1CYCLE) begin 
        id_inst = (id_valid_iaddr ? (id_update_flag!=id_update_flag_last ? ibus_rddata : id_inst_keep) : 32'h0);
    end else begin 
        id_inst = id_valid_iaddr ? id_ibus_rddata : 32'h0;
    end
end

id stage_id(/*autoinst*/
            .op(id_op),
            .op_type(id_op_type),
            .reg_s(id_reg_s),
            .reg_t(id_reg_t),
            .reg_d(id_reg_d),
            .immediate(id_immediate),
            .inst(id_inst),
            .pc_value(id_pc_value));

reg_val_mux reg_val_mux_s(/*autoinst*/
          .value_o(id_reg_s_value),
          .reg_addr(id_reg_s),
          .value_from_regs(id_reg_s_value_from_regs),
          .addr_from_ex(ex_reg_addr),
          .value_from_ex(ex_data_o),
          .access_op_from_ex(ex_mem_access_op),
          .addr_from_mm(mm_reg_addr_i),
          .value_from_mm(mm_data_i),
          .access_op_from_mm(mm_mem_access_op),
          .addr_from_wb(wb_reg_addr_i),
          .value_from_wb(wb_data_o),
          .write_enable_from_wb(wb_reg_we));

reg_val_mux reg_val_mux_t(/*autoinst*/
          .value_o(id_reg_t_value),
          .reg_addr(id_reg_t),
          .value_from_regs(id_reg_t_value_from_regs),
          .addr_from_ex(ex_reg_addr),
          .value_from_ex(ex_data_o),
          .access_op_from_ex(ex_mem_access_op),
          .addr_from_mm(mm_reg_addr_i),
          .value_from_mm(mm_data_i),
          .access_op_from_mm(mm_mem_access_op),
          .addr_from_wb(wb_reg_addr_i),
          .value_from_wb(wb_data_o),
          .write_enable_from_wb(wb_reg_we));

wire branch_taken;
assign id_branch_taken = ~id_cancel_instr & branch_taken;
branch branch_detect(/*autoinst*/
         .is_branch(id_is_branch),
         .branch_taken  (branch_taken),
         .branch_address(id_branch_address),
         .return_address(id_address),
         .inst(id_inst),
         .pc_value(id_pc_value),
         .reg_s_value(id_reg_s_value),
         .reg_t_value(id_reg_t_value));

always @(posedge clk) begin
    if (!rst_n || (!en_idex && en_exmm) || flush) begin
        ex_op <= `OP_SLL;
        ex_op_type <= `OPTYPE_R;
        ex_reg_s <= 5'b0;
        ex_reg_t <= 5'b0;
        ex_reg_d <= 5'b0;
        ex_reg_s_value <= 32'b0;
        ex_reg_t_value <= 32'b0;
        ex_immediate <= 20'b0;
        ex_address <= 32'b0;
        ex_pc_value <= 32'b0;
        ex_real_inst <= 1'b0;
    end
    else if(en_idex) begin
        ex_immediate <= id_immediate;
        ex_op_type <= id_op_type;
        ex_op <= id_cancel_instr ? `OP_SKIP : id_op;
        ex_reg_s <= id_reg_s;
        ex_reg_t <= id_reg_t;
        ex_reg_d <= id_reg_d;
        ex_address <= id_address;
        ex_reg_s_value <= id_reg_s_value;
        ex_reg_t_value <= id_reg_t_value;
        ex_pc_value <= id_pc_value;
        ex_real_inst <= id_real_inst & ~id_cancel_instr;
    end
end

ex stage_ex(/*autoinst*/
            .mem_access_op(ex_mem_access_op),
            .mem_access_sz(ex_mem_access_sz),
            .data_o(ex_data_o),
            .reg_addr(ex_reg_addr),
            .mem_addr(ex_mem_addr),
            .op(ex_op),
            .op_type(ex_op_type),
            .address(ex_address),
            .reg_s(ex_reg_s),
            .reg_t(ex_reg_t),
            .reg_d(ex_reg_d),
            .reg_s_value(ex_reg_s_value),
            .reg_t_value(ex_reg_t_value),
            .pc_value(ex_pc_value),
            .immediate(ex_immediate),
            .clk(clk),
            .rst_n(rst_n),
            .stall(ex_stall));

always @(posedge clk) begin
    if (!rst_n || (!en_exmm && en_mmwb) || flush) begin
        mm_mem_access_op <= `ACCESS_OP_D2R;
        mm_mem_access_sz <= `ACCESS_SZ_WORD;
        mm_data_i <= 32'b0;
        mm_reg_addr_i <= 5'b0;
        mm_addr_i <= 32'b0;
        mm_pc_value <= 32'b0;
        mm_real_inst <= 1'b0;
        mm_invalid_inst <= 1'b0;
    end
    else if(en_exmm) begin
        // $display("mm_pc_value<=%x", ex_pc_value);
        mm_mem_access_op <= ex_mem_access_op;
        mm_mem_access_sz <= ex_mem_access_sz;
        mm_data_i <= ex_data_o;
        mm_reg_addr_i <= ex_reg_addr;
        mm_addr_i <= ex_mem_addr;
        mm_pc_value <= ex_pc_value;
        mm_real_inst <= ex_real_inst;
        mm_invalid_inst <= ex_op == `OP_INVAILD;
    end
end

mm stage_mm(/*autoinst*/
            .data_o(mm_data_o),
            .mem_address(mm_mem_address),
            .mem_data_o(mm_mem_data_o),
            .mem_rd(mm_mem_rd),
            .mem_wr(mm_mem_wr),
            .mem_byte_en(mm_mem_byte_en),
            .mem_access_op(mm_mem_access_op),
            .mem_access_sz(mm_mem_access_sz),
            .alignment_err(mm_alignment_err),
            .data_i(mm_data_i),
            .reg_addr_i(mm_reg_addr_i),
            .addr_i(mm_addr_i),
            .exception_flush(flush));


always @(posedge clk) begin
    if (!rst_n || flush || !en_mmwb) begin
        wb_mem_access_op <= `ACCESS_OP_D2R;
        wb_mem_access_sz <= `ACCESS_SZ_WORD;
        wb_addr_i <= 32'b0;
        wb_data_i <= 32'b0;
        wb_reg_addr_i <= 5'b0;
        wb_dbus_rddata <= 32'h0;
    end
    else begin
        wb_mem_access_op <= mm_mem_access_op;
        wb_mem_access_sz <= mm_mem_access_sz;
        wb_addr_i <= mm_addr_i;
        wb_data_i <= mm_data_o;
        wb_reg_addr_i <= mm_reg_addr_i;
        wb_dbus_rddata <= dbus_rddata;
    end
end

always @(*) begin
    if(BUS_READ_1CYCLE)
        wb_mem_data_i = dbus_rddata;
    else
        wb_mem_data_i = wb_dbus_rddata;
end

wb stage_wb(/*autoinst*/
            .mem_access_sz(wb_mem_access_sz),
            .mem_data_i   (wb_mem_data_i),
            .addr_i       (wb_addr_i),
            .flag_unsigned (1'b0),
            .data_o       (wb_data_o),
            .reg_we(wb_reg_we),
            .mem_access_op(wb_mem_access_op),
            .data_i(wb_data_i),
            .reg_addr_i(wb_reg_addr_i));

endmodule

`default_nettype wire
